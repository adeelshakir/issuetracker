global.db_path = "<your_database_url>";

global.auth =  {
    user: '<your_valid_email>',
    pass: '<your_password>'
};
global.from = 'Admin TrackMyIssues';

global.host = 'http://trackmyissues.azurewebsites.net/';
//global.host = 'http://localhost:3000/';
