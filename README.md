# README #

### What is this repository for? ###

* Its the code for Issue Tracker application.
* Version 0.1

### Features of the Application ###

* Real-time board, changes are pushed immediately to all the connected clients.
* User can invite other users.
* User can create releases and sprints instantly.

### How do I get set up? ###

*  1.Download the Code
*  2.Set values in config/properties.js
*  3.Run the app with node.js

### Who do I talk to? ###

* Adeel Shakir - adeel.shakir@live.com